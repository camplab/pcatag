package edu.utah.camplab.pcatag.view;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import edu.utah.camplab.pcatag.constants.UIConstants;


public class OptionsPanel
  extends JPanel
{
  private static final int PANELS_WIDTH = 273;
  private static final int PANELS_HEIGHT = 150;
  private int optionTextHeight = 40;

  // contents of analysis options panel
  private JRadioButton m_haplotypeRadioButton = new JRadioButton(UIConstants.s_HAPLOTYPE);
  private JRadioButton m_genotypeRadioButton = new JRadioButton(UIConstants.s_GENOTYPE);
  private JCheckBox m_subsetAnalysisCheckBox = new JCheckBox(UIConstants.s_SUBSET_ANALYSIS);

  // PCA options panel
  private JTextField m_eigenValueThresholdTextField = new JTextField();
  private JTextField m_varianceExplainedTextField = new JTextField();

  // factor loading panel
  private JTextField m_retainTextField = new JTextField();
  private JTextField m_supressPrintBelowTextField = new JTextField();

  // advanced options panel
  private JCheckBox m_cumulativeEigenValuePlotGraphCheckBox = new JCheckBox(UIConstants.s_CUMULATIVE_EIGENVALUE_GRAPH);
  private JCheckBox m_screenPlotCheckBox = new JCheckBox(UIConstants.s_SCREE_PLOT);
  private JCheckBox m_correlationCovarianceMatCheckBox = new JCheckBox(UIConstants.s_CORRELATION_COVARIANCE_MATRICES);

  /*
   * 
   */
  public OptionsPanel( boolean genotype,
		       boolean subsetAnalysis,
		       double thresholdFactor,
		       double outputSupressThreshold,
		       double eigenValueThershold,
		       double varianceExplained)
  {
    createUI();
    m_haplotypeRadioButton.setSelected(!genotype);
    m_subsetAnalysisCheckBox.setSelected(subsetAnalysis);
    m_eigenValueThresholdTextField.setText("" + eigenValueThershold);
    m_varianceExplainedTextField.setText("" + varianceExplained);
    m_supressPrintBelowTextField.setText("" + outputSupressThreshold);
    m_retainTextField.setText("" + thresholdFactor);
  }

  private void createUI()
  {
    JPanel analysisOptionsPanel = new JPanel();
    {
      analysisOptionsPanel.setPreferredSize(new Dimension(PANELS_WIDTH-60, PANELS_HEIGHT));
      analysisOptionsPanel.setBorder(BorderFactory.createTitledBorder(UIConstants.s_ANALYSIS_OPTIONS));

      analysisOptionsPanel.setLayout(null);
      m_haplotypeRadioButton.setBounds(10, 20, 150, optionTextHeight);
      m_genotypeRadioButton.setBounds(10, 60, 150, optionTextHeight);
      m_subsetAnalysisCheckBox.setBounds(10, 100, 150, optionTextHeight);
      ButtonGroup group = new ButtonGroup();
      group.add(m_haplotypeRadioButton);
      group.add(m_genotypeRadioButton);
      analysisOptionsPanel.add(m_haplotypeRadioButton);
      analysisOptionsPanel.add(m_genotypeRadioButton);
      analysisOptionsPanel.add(m_subsetAnalysisCheckBox);
    }
    JPanel pcaAnalysisPanel = new JPanel();
    {
      JLabel m_eigenValueThresholdLabel = new JLabel(UIConstants.s_EIGENVALUE_THRESHOLD);
      JLabel m_varianceExplainedLabel = new JLabel(UIConstants.s_VARIANCE_EXPLAINED);

      pcaAnalysisPanel.setPreferredSize(new Dimension(PANELS_WIDTH + 60, PANELS_HEIGHT));
      pcaAnalysisPanel.setLayout(null);
      m_eigenValueThresholdLabel.setBounds(10, 20, 210, optionTextHeight);
      m_eigenValueThresholdTextField.setBounds(180, 25, 80, optionTextHeight);
      pcaAnalysisPanel.add(m_eigenValueThresholdTextField);
      pcaAnalysisPanel.add(m_eigenValueThresholdLabel);

      m_varianceExplainedLabel.setBounds(10, 60, 210, optionTextHeight);
      m_varianceExplainedTextField.setBounds(180, 65, 80, optionTextHeight);
      pcaAnalysisPanel.add(m_varianceExplainedTextField);
      pcaAnalysisPanel.add(m_varianceExplainedLabel);

      pcaAnalysisPanel.setBorder(BorderFactory
				 .createTitledBorder(UIConstants.s_PCA_PARAMETERS));
    }
    JPanel factorLoadingPanel = new JPanel();
    {
      JLabel m_retainLabel = new JLabel(UIConstants.s_RETAIN);
      JLabel m_supressPrintBelowLabel = new JLabel(UIConstants.s_SUPRESS_PRINT_BELOW);

      factorLoadingPanel.setPreferredSize(new Dimension(PANELS_WIDTH, PANELS_HEIGHT));

      m_retainLabel.setBounds(10, 20, 150, optionTextHeight);
      m_retainTextField.setBounds(160, 25, 80, optionTextHeight);
      factorLoadingPanel.add(m_retainLabel);
      factorLoadingPanel.add(m_retainTextField);

      m_supressPrintBelowLabel.setBounds(10, 60, 150, optionTextHeight);

      m_supressPrintBelowTextField.setBounds(160, 65, 80, optionTextHeight);

      factorLoadingPanel.add(m_supressPrintBelowLabel);
      factorLoadingPanel.add(m_supressPrintBelowTextField);

      factorLoadingPanel.setLayout(null);
      factorLoadingPanel.setBorder(BorderFactory
				   .createTitledBorder(UIConstants.s_FACTOR_LOADING));
    }
    JPanel advancedOptionsPanel = new JPanel();
    {
      advancedOptionsPanel.setPreferredSize(new Dimension(PANELS_WIDTH,
							  PANELS_HEIGHT));
      advancedOptionsPanel.setBorder(BorderFactory
				     .createTitledBorder(UIConstants.s_ADVANCED_OPTIONS));
      m_cumulativeEigenValuePlotGraphCheckBox.setEnabled(false);
      m_screenPlotCheckBox.setEnabled(false);
      m_correlationCovarianceMatCheckBox.setEnabled(false);

      advancedOptionsPanel.setLayout(null);
      m_cumulativeEigenValuePlotGraphCheckBox.setBounds(10, 20, 200, optionTextHeight);
      m_screenPlotCheckBox.setBounds(10, 60, 200, optionTextHeight);
      m_correlationCovarianceMatCheckBox.setBounds(10, 100, 200, optionTextHeight);
      advancedOptionsPanel.add(m_cumulativeEigenValuePlotGraphCheckBox);
      advancedOptionsPanel.add(m_screenPlotCheckBox);
      advancedOptionsPanel.add(m_correlationCovarianceMatCheckBox);
    }
    add(analysisOptionsPanel);
    add(pcaAnalysisPanel);
    add(factorLoadingPanel);
    add(advancedOptionsPanel);
  }

  public boolean isSubset()
  {
    return m_subsetAnalysisCheckBox.isSelected();
  }

  public boolean isGenotype()
  {
    return m_genotypeRadioButton.isSelected();
  }

  public double getEigenValue()
  {
    return Double.parseDouble(m_eigenValueThresholdTextField.getText());
  }

  public double getOutputSupressThreshold()
  {
    return Double.parseDouble(m_supressPrintBelowTextField.getText());
  }

  public double getVarianceExplained()
  {
    return Double.parseDouble(m_varianceExplainedTextField.getText());
  }

  public double getRetain()
  {
    return Double.parseDouble(m_retainTextField.getText());
  }
}
