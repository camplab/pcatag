package edu.utah.camplab.pcatag.utils;

import java.io.InputStream;
import java.io.InputStreamReader;


public class StreamReaderThread
    extends Thread
{
    public StringBuffer m_outBuffer;
    public InputStreamReader m_inBuffer;

    public StreamReaderThread(
        InputStream in,
        StringBuffer out)
    {
        m_outBuffer = out;
        m_inBuffer = new InputStreamReader(in);
    }

    @Override
    public void run()
    {
        int ch;
        try
        {
            while (-1 != (ch = m_inBuffer.read()))
            {
                m_outBuffer.append((char)ch);
            }
        }
        catch (Exception e)
        {
            m_outBuffer.append("\nRead error:" + e.getMessage());
        }
    }
}
