package edu.utah.camplab.pcatag.constants;

import java.util.ArrayList;


public class Constants
{

    public float a( ArrayList<Integer> list) {
        float a = 0;
        for (int i = 0; i < list.size(); i++)        {
            a += list.get(i).intValue();
        }
        return (a / list.size());
    }
}
