package edu.utah.camplab.pcatag.app;

import edu.utah.camplab.pcatag.constants.UIConstants;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;


public class RRunner {
  private File phaseFile;
  private double eigenValueThreshold = 0.7;
  private double retain = 0.4;
  private double suppress = 0.2;
  private String outputFileName = "";
  private boolean shortOutput = true;
  private boolean hasErrors = false;
  private ArrayList<String> tSNPs = new ArrayList<String>();
  private ArrayList<String> SNPs = new ArrayList<String>();
  private int LDGroups = 0;
  private int totalLDGroups = 0;
  private int numMarkers = 0;
  private int subset = 0;
  private DecimalFormat prec4 = new DecimalFormat("0.0000");
  private DecimalFormat prec2 = new DecimalFormat("00.00");
  private String[][] table1Output;
  private String[] CumVar;
  private double varThreshHold = 90;

  public RRunner(File pfile,
                 int nmarkers,
                 double threshold,
                 double ret,
                 double supp,
                 String output,
                 boolean shortOut,
                 double varthresh,
                 int sub) {
    phaseFile		= pfile;
    eigenValueThreshold = threshold;
    retain		= ret;
    suppress		= supp;
    outputFileName	= output;
    shortOutput		= shortOut;
    numMarkers		= nmarkers;
    subset		= sub;
  }

  public boolean run(DataAnalyzer dataAnalyzser) {
    hasErrors = false;

    String RFile = writeRFileStep1();
    LinkedHashMap<String, Double[]>[] step1ResultList = (LinkedHashMap<String, Double[]>[]) new  LinkedHashMap[3];
    step1ResultList = execRStep1(RFile, true);

    if (!hasErrors) {
      printTable1();
    }

    if (!shortOutput && !hasErrors) {
      printLDGroupOutput(step1ResultList[0], step1ResultList[1], 0);
    }
    execRStep2(step1ResultList, dataAnalyzser);
    if (!hasErrors) {
      printSummaryTable();
    }
    return !hasErrors;
  }

  private String writeRFileStep1() {
    FileOutputStream fout;
    File routFile = null;

    try {
      // Open an output stream
      routFile = File.createTempFile("Rfile", ".R");
      System.out.println("Writing R file: " + routFile.getCanonicalPath());
      fout = new FileOutputStream(routFile);
      // Print a line of text
      new PrintStream(fout).println("library(FactoMineR)");
      new PrintStream(fout).println("out1=read.table(\"" + phaseFile.getAbsolutePath() + "\", header=TRUE)");
      new PrintStream(fout).println("out1=out1[,1:" + numMarkers + "]");
      new PrintStream(fout).println("p1=PCA(out1,graph=FALSE)");
      new PrintStream(fout).println("keep=sum(p1$eig[,'eigenvalue']>=" + eigenValueThreshold + ")");
      new PrintStream(fout).println("p2=PCA(out1, graph=FALSE, ncp=keep, row.w=out1$Efreq)");
      new PrintStream(fout).println("options(width=10000)");
      new PrintStream(fout).println("varimax(p2$var$cor)");
      new PrintStream(fout).println("p2$eig");

      // Close our output stream
      fout.close();
    }
    // Catches any error conditions
    catch (IOException e) {
      System.err.println("Unable to write to file");
      System.exit(-1);
    }

    return routFile.getAbsolutePath();
  }

  private void execRStep2( LinkedHashMap<String, Double[]>[] step1ResultList, DataAnalyzer analyzer) {

    LinkedHashMap<String, Double[]> step1Loadings = step1ResultList[0];
    ArrayList<String> validFactors = getValidFactors(step1Loadings);
    LinkedHashMap<String, Double[]>[] result = (LinkedHashMap<String, Double[]>[]) new LinkedHashMap[3];

    FileOutputStream fout;

    if (validFactors == null) {
      return;
    }

    tSNPs.clear();
    SNPs.clear();

    for (int i = 0; i < LDGroups; i++) {
      tSNPs.add("");
      SNPs.add("");
    }

    for (int i = 0; i < validFactors.size(); i++) {
      String validColumns = validFactors.get(i);

      String[] SNPs = validColumns.replace(",c(", "").replace(")", "").split(",");

      if (SNPs.length == 0 || validColumns == "") {
        continue;
      }
      else if (SNPs.length == 1) {
        String key = analyzer.getValueFromIndex(step1Loadings, Integer.parseInt(SNPs[0]));
        String markers = tSNPs.get(i + 1);

        if (markers != "") {
          tSNPs.add(markers + "," + key);
        }
        else {
          tSNPs.set(i + 1, key);
        }
      }
      else if (SNPs.length == 2) {
        String key1 = analyzer.getValueFromIndex(step1Loadings, Integer.parseInt(SNPs[0]));
        String key2 = analyzer.getValueFromIndex(step1Loadings, Integer.parseInt(SNPs[1]));
        Double factor1 = analyzer.getFactor(step1Loadings, Integer.parseInt(SNPs[0]), i);
        Double factor2 = analyzer.getFactor(step1Loadings, Integer.parseInt(SNPs[1]), i);

        if (Math.abs(factor1) >= Math.abs(factor2)) {
          String markers = tSNPs.get(i + 1);

          if (markers != "") {
            tSNPs.set(i + 1, markers + "," + key1);
          }
          else {
            tSNPs.set(i + 1, key1);
          }
        }
        else {
          String markers = tSNPs.get(i + 1);
          if (markers != "") {
            tSNPs.set(i + 1, markers + "," + key2);
          }
          else {
            tSNPs.set(i + 1, key2);
          }
        }
      }
      else {
        try {
          // Open an output stream
	  File routFile = File.createTempFile("Rfile", ".R");
          fout = new FileOutputStream(routFile);

          // Print a line of text
          new PrintStream(fout).println("library(FactoMineR)");
          new PrintStream(fout).println("out1=read.table(\"" + phaseFile.getAbsolutePath() + "\", header=TRUE)");
          new PrintStream(fout).println("out1=out1[,1:" + numMarkers + "]");
          new PrintStream(fout).println("p1=PCA(out1[" + validColumns + "], graph=FALSE)");
          new PrintStream(fout).println("keep=sum(p1$eig$eigenvalue>=" + eigenValueThreshold + ")");
          new PrintStream(fout).println("p2=PCA(out1, graph=FALSE, ncp=keep, row.w=out1$Efreq)");
          new PrintStream(fout).println("options(width=10000)");
          new PrintStream(fout).println("varimax(p2$var$cor)");

          // Close our output stream
          fout.close();

          result = execRStep1("RFile.r",
            false);

          if (!shortOutput && !hasErrors)
          // printLDGroupTables(result[0],result[1],i + 1);
          {
            printLDGroupOutput(result[0],
              result[1],
              i + 1);
          }

          prepareSNPData(result[0],
            SNPs,
            i);
        }
        // Catches any error conditions
        catch (IOException e) {
          System.err.println("Unable to write to file");
          System.exit(-1);
        }
      }
    }
  }

  private ArrayList<String> getValidFactors(
    LinkedHashMap<String, Double[]> step1Loadings) {
    String validRows = ",c(";
    ArrayList<String> result = new ArrayList<String>();
    int row = 0;

    if (step1Loadings == null || step1Loadings.size() == 0) {
      return null;
    }

    for (int i = 0; i < LDGroups; i++) {
      result.add("");
    }

    for (int i = 0; i < LDGroups; i++) {
      Iterator<String> iter = step1Loadings.keySet().iterator();

      validRows = ",c(";
      row = 0;

      while (iter.hasNext()) {
        String key = iter.next();
        Double[] values = step1Loadings.get(key);

        if (values != null) {
          if (values[i] != null) {
            if (Math.abs(values[i]) >= retain) {
              if (validRows.equals(",c(")) {
                validRows = validRows + row;
              }
              else {
                validRows = validRows + "," + row;
              }
            }
          }
          row++;
        }
      }

      if (!validRows.equals(",c(")) {
        result.set(i,
          validRows + ")");
      }
      else {
        result.set(i,
          "");
      }
    }

    return result;
  }

  private LinkedHashMap<String, Double[]>[] execRStep1(String RFile, boolean step1) {
    LinkedHashMap<String, Double[]>[] result = null;

    try {

      String execCommand = PCATagView.s_R_EXECUTABLE_FILE_NAME + " --no-restore --no-save -f " + RFile;

      // System.out.println("Executing: " + exec);
      // System.out.println();

      if (!RFile.equals("")) {
        System.out.println(execCommand);
        Process p = Runtime.getRuntime().exec(execCommand);
        BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
        BufferedReader err = new BufferedReader(new InputStreamReader(p.getErrorStream()));

        result = parseR(in, step1);

        String line;
        while ((line = err.readLine()) != null) {
          hasErrors = true;
          System.out.println(line);
        }
      }
    } catch (IOException ioe) {
      throw new RuntimeException(ioe);
    }

    return result;
  }

  private LinkedHashMap<String, Double[]>[] parseR( BufferedReader in, boolean step1) {
    String line = null;
    LinkedHashMap<String, Double[]> loadings = new LinkedHashMap<String, Double[]>();
    LinkedHashMap<String, Double[]> loadings2 = new LinkedHashMap<String, Double[]>();
    LinkedHashMap<String, Double[]> rotmat = new LinkedHashMap<String, Double[]>();
    LinkedHashMap<String, Double[]> summary = new LinkedHashMap<String, Double[]>();

    try {
      int output = 1;

      while ((line = in.readLine()) != null) {
        if (line.contains("Error")) {
          hasErrors = true;
        }
        if (line.trim().equals("Loadings:")) {
          while ((line = in.readLine()) != null) {
            String dimLine = line;
            // System.out.println(line);

            LDGroups = line.split("\\s+").length;
            if (totalLDGroups == 0) {
              totalLDGroups = LDGroups;
            }

            if ((line = in.readLine()) != null) {
              while (line != null) {
                if (line.equals("")) {
                  // System.out.println(line);
                  line = in.readLine();
                  if (line != null) {
                    line = in.readLine();
                    if (line != null
                      && line.trim().contains("[,1]")) {
                      // System.out.println(line);
                      line = in.readLine();
                    }
                    if (line != null
                      && line.trim()
                      .contains("eigenvalue")) {
                      // System.out.println(line);
                      line = in.readLine();
                    }
                    output++;
                  }
                  continue;
                }

                if (output == 4) {
                  if (line.trim().equals(">")) {
                    break;
                  }

                  line = line.replace("comp ",
                    "");
                }

                // System.out.println(line);

                String[] lineItems = line.split("\\s+");

                int columnsToSkip = columnsToSkip(lineItems);
                Double[] values = new Double[LDGroups];

                if (output == 1) {
                  line = line.substring(dimLine.indexOf('D'));
                  int index = 0;
                  for (int i = 0; i < line.length(); i = i + 7) {
                    if (line.substring(i,
                      i + 6).trim().equals("")) {
                      values[index] = null;
                    }
                    else {
                      values[index] = Double
                        .parseDouble(line.substring(i,
                          i + 6));
                    }

                    index++;
                  }
                }
                else {
                  if (output == 4) {
                    columnsToSkip = 1;
                  }

                  values = new Double[lineItems.length
                    - columnsToSkip];

                  for (int i = columnsToSkip; i < lineItems.length; i++) {
                    if (lineItems[i].equals("")) {
                      values[i - columnsToSkip] = null;
                    }
                    else {
                      values[i - columnsToSkip] = Double
                        .parseDouble(lineItems[i]);
                    }
                  }
                }
                if (output == 1) {
                  loadings.put(getKey(lineItems,
                    columnsToSkip).trim(),
                    values);
                }
                else if (output == 2) {
                  loadings2.put(getKey(lineItems,
                    columnsToSkip).trim(),
                    values);
                }
                else if (output == 3) {
                  rotmat.put(getKey(lineItems,
                    columnsToSkip).trim(),
                    values);
                }
                else if (output == 4) {
                  summary.put(getKey(lineItems,
                    columnsToSkip).trim(),
                    values);
                }

                line = in.readLine();
              }

              break;
            }
          }
        }
      }

      if (step1) {
        table1Output = prepareSummaryArray(summary);
      }

      // DataAnalyzer.printDoubleMap(loadings);
      // DataAnalyzer.printDoubleMap(loadings2);
      // DataAnalyzer.printDoubleMap(rotmat);
      // DataAnalyzer.printDoubleMap(summary);
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
    LinkedHashMap<String, Double[]>[] result = (LinkedHashMap<String, Double[]>[]) new LinkedHashMap[3];
    result[0] = loadings;
    result[1] = loadings2;
    result[2] = rotmat;
    return result;
  }

  private int columnsToSkip(
    String[] lineItems) {
    int columnsToSkip = 0;

    for (int i = 0; i < lineItems.length; i++) {
      try {
        Double.parseDouble(lineItems[i]);
        break;
      } catch (Exception e) {
        columnsToSkip++;
        continue;
      }
    }

    return columnsToSkip;
  }

  private String getKey(
    String[] lineItems,
    int columnsToSkip) {
    String key = "";

    for (int i = 0; i < columnsToSkip; i++)
      key += " " + lineItems[i];

    return key;
  }

  private void printLDGroupOutput(
    LinkedHashMap<String, Double[]> loadings,
    LinkedHashMap<String, Double[]> loadings2,
    int index) {
    if (loadings == null || loadings2 == null || hasErrors) {
      return;
    }

    FileOutputStream fout;

    try {

      String newline = System.getProperty("line.separator");

      FileWriter writer = new FileWriter(outputFileName, true);

      int rows = numMarkers + 4;
      int columns = 0;
      if (index == 0) {
        columns = totalLDGroups + 1;
      }
      else {
        columns = LDGroups + 1;
      }

      String[][] output = new String[rows][columns];

      int[] maxLen = new int[totalLDGroups + 1];

      PrintWriter pw = new PrintWriter(writer);
      PrintWriter pw2 = new PrintWriter(System.out, true);
      if (index == 0) {
        writer.write("LD group designation: " + newline + newline);
        System.out.print("LD group designation: " + newline + newline);
      }
      else {
        writer.write("LD group " + index + ": " + newline + newline);
        System.out
          .print("LD group " + index + ": " + newline + newline);
      }
      output[0][0] = "SNP";

      int row = 1;

      if (index == 0) {
        for (int i = 1; i < totalLDGroups; i++)
          output[0][i] = "LD Group " + i;
      }
      else {
        for (int i = 1; i < LDGroups; i++)
          output[0][i] = "Subgroup " + i;
      }

      Iterator<String> iter = loadings.keySet().iterator();

      while (iter.hasNext()) {
        String key = iter.next();
        Double[] values = loadings.get(key);

        output[row][0] = key;

        for (int i = 0; i < values.length; i++) {
          if (values[i] == null) {
            output[row][i + 1] = "";
          }
          else if (Math.abs(values[i]) >= suppress
            && Math.abs(values[i]) < retain) {
            output[row][i + 1] = "[" + prec4.format(values[i])
              + "]";
          }
          else if (Math.abs(values[i]) >= retain) {
            output[row][i + 1] = prec4.format(values[i]);
          }
          else {
            output[row][i + 1] = "";
          }
        }

        row++;
      }

      if (index == 0) {
        for (int j = 0; j < totalLDGroups + 1; j++)
          output[row][j] = "";
      }
      else {
        for (int j = 0; j < LDGroups + 1; j++)
          output[row][j] = "";
      }

      row++;

      iter = loadings2.keySet().iterator();
      if (iter.hasNext()) {
        iter.next();
        while (iter.hasNext()) {
          String key = iter.next();
          Double[] values = loadings2.get(key);

          output[row][0] = key + "(%)";

          for (int i = 0; i < values.length; i++) {
            output[row][i + 1] = "" + prec2.format(values[i] * 100);
          }

          row++;
        }
      }

      for (int i = 0; i < columns; i++) {
        maxLen[i] = 0;

        for (int j = 0; j < rows; j++)
          if (output[j][i] != null) {
            if (output[j][i].length() > maxLen[i]) {
              maxLen[i] = output[j][i].length();
            }
          }
      }

      String format = "";

      for (int j = 0; j < columns; j++)
        format = format + "%-" + (maxLen[j] + 4) + "s";

      format = format + "%n";

      String[] toBePrinted = new String[columns];

      for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
          if (output[i][j] != null) {
            toBePrinted[j] = output[i][j];
          }
          else {
            toBePrinted[j] = "";
          }
        }

        pw.printf(format,
          (Object[]) toBePrinted);
        pw2.printf(format,
          (Object[]) toBePrinted);
      }

      writer.write(newline + newline);
      System.out.println(newline);

      // Close our output stream
      pw.close();
      writer.close();

    }
    // Catches any error conditions
    catch (IOException e) {
      System.err.println("Unable to write to file");
      System.exit(-1);
    }
  }

  private void printLDGroupTables(
    LinkedHashMap<String, Double[]> loadings,
    LinkedHashMap<String, Double[]> loadings2,
    int index) {
    if (loadings == null || loadings2 == null || hasErrors) {
      return;
    }

    String newline = System.getProperty("line.separator");

    try {
      // Open an output stream
      FileWriter writer = new FileWriter(outputFileName, true);

      writer.write(newline + newline);

      writer.write("LD group " + index + ": " + newline + newline);
      System.out.print("LD group " + index + ": " + newline + newline);
      writer.write("SNP");
      System.out.print("SNP");

      for (int i = 1; i < LDGroups; i++) {
        writer.write("\tSubgroup " + i);
        System.out.print("\tSubgroup " + i);
      }

      writer.write(newline);
      System.out.println();

      Iterator<String> iter = loadings.keySet().iterator();

      while (iter.hasNext()) {
        String key = iter.next();
        Double[] values = loadings.get(key);

        writer.write(key);
        System.out.print(key);

        for (int i = 0; i < values.length; i++) {
          if (values[i] == null) {
            writer.write("\t\t");
            System.out.print("\t\t");
          }
          else if (Math.abs(values[i]) >= suppress
            && Math.abs(values[i]) < retain) {
            writer.write("\t[" + prec4.format(values[i]) + "]");
            System.out.print("\t[" + prec4.format(values[i]) + "]");
          }
          else if (Math.abs(values[i]) >= retain) {
            writer.write("\t" + prec4.format(values[i]));
            System.out.print("\t" + prec4.format(values[i]));
          }
          else {
            writer.write("\t\t");
            System.out.print("\t\t");
          }
        }
        writer.write(newline);
        System.out.println();
      }

      writer.write(newline);
      System.out.println();

      iter = loadings2.keySet().iterator();
      if (iter.hasNext()) {
        iter.next();
        while (iter.hasNext()) {
          String key = iter.next();
          Double[] values = loadings2.get(key);

          writer.write(key + "(%)");
          System.out.print(key + "(%)");

          for (int i = 0; i < values.length; i++) {
            writer
              .write("\t"
                + Double.parseDouble(prec4
                .format(values[i] * 100)));
            System.out
              .print("\t"
                + Double.parseDouble(prec4
                .format(values[i] * 100)));
          }

          writer.write(newline);
          System.out.println();
        }
      }

      // Close our output stream
      writer.close();

    }
    // Catches any error conditions
    catch (IOException e) {
      System.err.println("Unable to write to file");
      System.exit(-1);
    }
  }

  private void prepareSNPData(
    LinkedHashMap<String, Double[]> loadings,
    String[] SNP,
    int index) {
    Double max = 0.0;
    int maxIndex = 0;
    String key = "";

    for (int i = 0; i < SNP.length; i++) {
      Double factor = DataAnalyzer.getFactor(loadings,
        Integer.parseInt(SNP[i]),
        index);

      if (factor == null) {
        continue;
      }

      if (Math.abs(factor) >= Math.abs(max)) {
        max = factor;
        maxIndex = i;
      }

      if (Math.abs(factor) >= retain) {
        key = DataAnalyzer.getValueFromIndex(loadings,
          Integer.parseInt(SNP[i]));

        String markers = SNPs.get(index + 1);

        if (markers != "") {
          SNPs.set(index + 1,
            markers + "," + key);
        }
        else {
          SNPs.set(index + 1,
            key);
        }
      }

    }

    key = DataAnalyzer.getValueFromIndex(loadings,
      Integer.parseInt(SNP[maxIndex]));

    String markers = tSNPs.get(index + 1);

    if (markers != "") {
      tSNPs.set(index + 1,
        markers + "," + key);
    }
    else {
      tSNPs.set(index + 1,
        key);
    }
  }

  private void printTable1() {
    try {

      String newline = System.getProperty("line.separator");

      FileWriter writer = null;

      if (subset == 0) {
        writer = new FileWriter(outputFileName);
      }
      else {
        writer = new FileWriter(outputFileName, true);
      }

      int columns = 4;

      int[] maxLen = new int[4];

      PrintWriter pw = new PrintWriter(writer);
      PrintWriter pw2 = new PrintWriter(System.out, true);

      if (subset == 0) {
        writer.write(newline + UIConstants.pca_OVERALL + newline);
        writer.write("-------" + newline + newline);
        System.out.print(newline + UIConstants.pca_OVERALL + newline);
        System.out.print("-------" + newline + newline);
      }
      else if (subset == 1) {
        writer.write(newline + newline + UIConstants.pca_CONTROL + newline);
        writer.write("-------" + newline + newline);
        System.out.print(newline + newline + UIConstants.pca_CONTROL + newline);
        System.out.print("-------" + newline + newline);
      }
      else if (subset == 2) {
        writer.write(newline + newline + UIConstants.pca_CASE + newline);
        writer.write("----" + newline + newline);
        System.out.print(newline + newline + UIConstants.pca_CASE + newline);
        System.out.print("----" + newline + newline);
      }

      writer.write("Summary of PCA factors: " + newline + newline);
      System.out.print("Summary of PCA factors: " + newline + newline);

      for (int i = 0; i < columns; i++) {
        maxLen[i] = 0;

        for (int j = 0; j < totalLDGroups + 1; j++)
          if (table1Output[j][i] != null) {
            if (table1Output[j][i].length() > maxLen[i]) {
              maxLen[i] = table1Output[j][i].length();
            }
          }
      }

      String format = "";

      for (int j = 0; j < columns; j++)
        format = format + "%-" + (maxLen[j] + 4) + "s";

      format = format + "%n";

      String[] toBePrinted = new String[columns];

      for (int i = 0; i < totalLDGroups + 1; i++) {
        for (int j = 0; j < columns; j++) {
          if (table1Output[i][j] != null) {
            if (table1Output[i][j].equals("")) {
              toBePrinted[j] = table1Output[i][j];
            }
            else {
              if (j > 1 && i > 0) {
                toBePrinted[j] = prec2.format(Double
                  .parseDouble(table1Output[i][j]));
              }
              else {
                toBePrinted[j] = table1Output[i][j];
              }
            }
          }
          else {
            toBePrinted[j] = "";
          }
        }

        pw.printf(format,
          (Object[]) toBePrinted);
        pw2.printf(format,
          (Object[]) toBePrinted);
      }

      writer.write(newline + newline);
      System.out.println(newline);

      // Close our output stream
      pw.close();
      writer.close();

    }
    // Catches any error conditions
    catch (IOException e) {
      System.err.println("Unable to write to file");
      System.exit(-1);
    }

  }

  private void printSummaryTable() {
    String newline = System.getProperty("line.separator");

    try {
      // Open an output stream
      FileWriter writer = new FileWriter(outputFileName, true);

      if (!shortOutput) {
        writer.write(newline + newline);
        System.out.println(newline);
      }
      writer.write("Final Summary: " + newline + newline);
      System.out.print("Final Summary: " + newline + newline);

      PrintWriter pw = new PrintWriter(writer);
      PrintWriter pw2 = new PrintWriter(System.out, true);

      String[] ldgroup = new String[totalLDGroups];
      String[] snps = new String[totalLDGroups];
      String[] tsnps = new String[totalLDGroups];
      String[] variance = new String[totalLDGroups];

      int ldgroupMaxLen = 8;
      int snpsMaxLen = 4;
      int tsnpsMaxLen = 5;
      int varianceMaxLen = 20;

      for (int i = 0; i < totalLDGroups; i++) {
        ldgroup[i] = "" + (i + 1);
        if (ldgroup[i].length() > ldgroupMaxLen) {
          ldgroupMaxLen = ldgroup[i].length();
        }
        snps[i] = SNPs.get(i);
        if (snps[i].length() > snpsMaxLen) {
          snpsMaxLen = snps[i].length();
        }
        tsnps[i] = tSNPs.get(i);
        if (tsnps[i].length() > tsnpsMaxLen) {
          tsnpsMaxLen = tsnps[i].length();
        }

        if (i <= CumVar.length) {
          if (CumVar[i] != null) {
            variance[i] = CumVar[i];
          }
          else {
            variance[i] = "";
          }
        }
        else {
          variance[i] = "";
        }

        if (variance[i].length() > varianceMaxLen) {
          varianceMaxLen = variance[i].length();
        }
      }

      final String SUMMARY_FORMAT = "%-" + (ldgroupMaxLen + 4) + "s%-"
        + (snpsMaxLen + 4) + "s%-" + (tsnpsMaxLen + 4) + "s%-"
        + (varianceMaxLen + 4) + "s%n";

      pw.printf(SUMMARY_FORMAT,
        "LD Group",
        "SNPs",
        "tSNPs",
        "%Cumulative Variance");
      pw2.printf(SUMMARY_FORMAT,
        "LD Group",
        "SNPs",
        "tSNPs",
        "%Cumulative Variance");

      for (int i = 0; i < totalLDGroups; i++) {
        pw.printf(SUMMARY_FORMAT,
          ldgroup[i],
          snps[i],
          tsnps[i],
          variance[i]);
        pw2.printf(SUMMARY_FORMAT,
          ldgroup[i],
          snps[i],
          tsnps[i],
          variance[i]);
      }

      writer.write(newline);
      System.out.println();

      // Close our output stream
      pw.close();
      writer.close();

    }
    // Catches any error conditions
    catch (IOException e) {
      System.err.println("Unable to write to file");
      System.exit(-1);
    }
  }

  private String[][] prepareSummaryArray(
    LinkedHashMap<String, Double[]> map) {
    int row = 1;

    String[][] result = new String[map.size() + 1][4];
    CumVar = new String[totalLDGroups];
    double variance = 0.0;
    int stop = 0;

    result[0][0] = "LD Group";
    result[0][1] = "Eigenvalue";
    result[0][2] = "% Individual Variance";
    result[0][3] = "% Cumulative Variance";

    if (map != null && !map.isEmpty()) {
      Iterator<String> iter = map.keySet().iterator();

      while (iter.hasNext()) {
        if (stop == 2) {
          break;
        }

        String key = iter.next();

        Double[] value = map.get(key);

        result[row][0] = "" + key;
        // System.out.println(result[row][0]);

        for (int column = 1; column < value.length + 1; column++) {
          result[row][column] = "" + value[column - 1];
          // System.out.println(result[row][column]);

          if (column == 3) {
            variance = value[column - 1];

            if (row <= CumVar.length) {
              CumVar[row - 1] = "" + prec2.format(variance);
            }

            if (variance > Math.round(varThreshHold)) {
              stop++;
              if (stop == 2) {
                break;
              }
            }
          }
        }
        row++;
      }
    }
    return result;
  }
}
