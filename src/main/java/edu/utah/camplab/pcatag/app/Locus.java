package edu.utah.camplab.pcatag.app;

public class Locus {
  private String Allele1;
  private String Allele2;
  private String Name;

  public Locus() {
    Name = "";
    Allele1 = "";
    Allele2 = "";
  }

  public Locus(String Name, String Allele1, String Allele2) {
    this.Name = Name;
    this.Allele1 = Allele1;
    this.Allele2 = Allele2;
  }

  public void SetAllele1(String Allele1) {
    this.Allele1 = Allele1;
  }

  public String GetAllele1() {
    return this.Allele1;
  }

  public void SetAllele2(String Allele2) {
    this.Allele2 = Allele2;
  }

  public String GetAllele2() {
    return this.Allele2;
  }

  public String GetName() {
    return this.Name;
  }

  public void printLocus() {
    System.out.print(this.Allele1 + " " + this.Allele2 + " ");
  }

  public String genotypeDecode() {
    // 1 1 converts to -1
    // 1 2 or 2 1 converts to 0
    // 2 2 converts to 1

    if (this.Allele1.equals(this.Allele2)) {
      if (this.Allele1.equals("1")) {
        return "-1";
      }
      else if (this.Allele1.equals("2")) {
        return "1";
      }
    }
    // I suspect else return "0" would do the trick
    else if ((this.Allele1.equals("1") && this.Allele2.equals("2"))
      || (this.Allele1.equals("2") && this.Allele2.equals("1"))
      || (this.Allele1.equals("0") && this.Allele2.equals("0"))) {
      return "0";
    }
    return "0";
  }
}
