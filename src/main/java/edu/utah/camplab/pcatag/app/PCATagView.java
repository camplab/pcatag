package edu.utah.camplab.pcatag.app;

import java.awt.Cursor;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import edu.utah.camplab.pcatag.utils.StringUtils;
import edu.utah.camplab.pcatag.view.InputOutputFilesPanel;
import edu.utah.camplab.pcatag.view.OptionsPanel;
import edu.utah.camplab.pcatag.components.ExitableApplicationFrame;
import edu.utah.camplab.pcatag.components.VerticalLayout;
import edu.utah.camplab.pcatag.constants.UIConstants;
import edu.utah.camplab.pcatag.io.TextAreaOutputStream;
import edu.utah.camplab.pcatag.utils.StreamReaderThread;

public class PCATagView {
  private static final String s_appFileName = "pcaTag.jar";

  // files names
  public static final String s_PHASE_EXECUTABLE_FILE_NAME = "fastPHASE";
  public static final String s_R_EXECUTABLE_FILE_NAME = "R";

  // temp directory where jars are extracted
  private static String s_binariesDirNameBase = "redist";
  public static String s_binariesDirNameRuntime = "./" + s_binariesDirNameBase;
  private static String s_binariesDirNameExtraction = s_binariesDirNameBase;

  /**
   * options
   */
  private static boolean m_uiMode = false;
  private static boolean m_analizeGenotype = false;
  private static boolean m_requiresSubsetAnalysis = false;
  private static boolean m_longPrintMode = false;
  private static String m_inputFileName = "";
  private static String m_outputFileName = "";
  private static String m_workingDirectory = System.getProperty("user.dir");
  private static double m_thresholdFactor = 0.4;
  private static double m_outputSupressThreshold = 0.2;
  private static double m_eigenValueThershold = 0.7;
  private static double m_varianceExplained = 90;

  // UI Variables
  private ExitableApplicationFrame m_frame = null;
  private OptionsPanel m_optionsPanel = null;
  private InputOutputFilesPanel m_ioPanel = null;
  private static JTextArea area = null;

  /**
   * @param args
   */ public static void main( String args[]) {
     /**
      * All other parameters
      */
     int filesNamed = 0;
     if (args.length == 0 ) {
       System.out.println(" NO ARGS");
       showHelp();
     }
     for (int i = 0; i < args.length; i++) {
       if (args[i].equals("-i")) {        // input file name
	 m_inputFileName = args[++i];
	 filesNamed++;
       }
       else if (args[i].equals("-o")) { // output file name
	 m_outputFileName = args[++i];
	 filesNamed++;
       }
       else if (args[i].equals("-d")) { // output file name
         m_workingDirectory = args[++i];
         filesNamed++;
       }
       else if (args[i].equals("-h")) { // output file name
	 showHelp();
       }
       else if (args[i].equals("-g")) { // analyze genotype default is heplotype
	 m_analizeGenotype = true;
       }
       else if (args[i].equals("-f")) { // -f threshold for the factor deafault is 0.4
	 m_thresholdFactor = Double.parseDouble(args[++i]);
       }
       else if (args[i].equals("-t")) { // threshold below which output is suppressed in output tables
	 m_outputSupressThreshold = Double.parseDouble(args[++i]);        // (default 0.2)
       }
       else if (args[i].equals("-e")) { // threshold for eigenvalue for including LD groups (default=0.7)
	 m_eigenValueThershold = Double.parseDouble(args[++i]);
       }
       else if (args[i].equals("-v")) { // % variance explained (default 90%)
	 m_varianceExplained = Double.parseDouble(args[++i]);
       }
       else if (args[i].equals("-s")) { // if require subset analysis (default ignores subgroup column)
	 m_requiresSubsetAnalysis = true;
       }
       else if (args[i].equals("-p")) {
	 i++;
	 if (args[i].equals("short")) {
	   m_longPrintMode = false;
	 }
	 else if (args[i].equals("long")) {
	   m_longPrintMode = true;
	 }
	 else {
	   System.out.println("Invalid print mode " + args[i]
			      + " for parameter -p valid modes are (short,long)\n");
	   System.exit(1);
	 }
       }
       else if (args[i].equals("-u")) { // UI mode
	 m_uiMode = true;
       }
     }
     if (filesNamed < 2 && ! m_uiMode) {
       showHelp();
     }

     setNativeLookAndFeel();
     new PCATagView(m_uiMode,
		    m_analizeGenotype,
		    m_requiresSubsetAnalysis,
		    m_longPrintMode,
		    m_inputFileName,
		    m_outputFileName,
		    m_thresholdFactor,
		    m_outputSupressThreshold,
		    m_eigenValueThershold,
		    m_varianceExplained);
   }

  public static void extractBinariesFast() {
    try {
      Runtime runtime = Runtime.getRuntime();
      String extractCommand = "jar -xf " + s_appFileName + " "
        + s_binariesDirNameExtraction;

      Process proc = runtime.exec(extractCommand);

      StreamReaderThread outThread = new StreamReaderThread(proc
							    .getInputStream(), new StringBuffer());
      StreamReaderThread errThread = new StreamReaderThread(proc
							    .getErrorStream(), new StringBuffer());

      // start both threads
      outThread.start();
      errThread.start();

      // wait for process to end
      proc.waitFor();

      // finish reading whatever's left in the buffers
      outThread.join();
      errThread.join();
    } catch (IOException ioe) {

    } catch (InterruptedException ie) {

    }

  }

  public static void extractBinariesSlow() {
    try {
      JarFile jar = new JarFile(s_appFileName);
      Enumeration enumer = jar.entries();
      int totFiles = jar.size();
      int filesExtractedSoFar = 0;

      System.out
        .println("Extracting files needed to run PCTag, please wait this will take might take a few minuites");
      while (enumer.hasMoreElements()) {
        filesExtractedSoFar++;
        System.out.println("Extracting file " + filesExtractedSoFar + " of " + totFiles + " files");

        JarEntry file = (JarEntry) enumer.nextElement();
        File f = new File(s_binariesDirNameRuntime + "/" + file.getName());
        if (file.isDirectory()) { // if its a directory, create it
          f.mkdirs();
          continue;
        }
        InputStream is = jar.getInputStream(file);
        FileOutputStream fos = new FileOutputStream(f);

        while (is.available() > 0) {
          fos.write(is.read());
        }
        fos.close();
        is.close();
      }

      // we want to delete 2 directories that are extracted and not needed
      File fileToDelete = new File(s_binariesDirNameRuntime + "/" + "META-INF");
      fileToDelete.delete();
      fileToDelete = new File(s_binariesDirNameRuntime + "/" + "com");
      fileToDelete.delete();
    } catch (IOException ioe) {
      throw new RuntimeException(ioe);
    }
  }

  private static void showHelp() {
    System.out.print("REQUIRED:\n");
    System.out.print(String.format("%s\t%s\n", "-i", "the filename of input genotypes"));
    System.out.print(String.format("%s\t%s\n", "-o", "the output filename"));
    System.out.print("Defaulted:\n");
    System.out.print(String.format("%s\t%s\n", "-d <file path>", "<working directory>, (default current dir)"));
    System.out.print(String.format("%s\t%s\n", "-e <decimal fraction>", "threshold for eigenvalue for including LD groups (default 0.7)"));
    System.out.print(String.format("%s\t%s\n", "-f <decimal fraction>", "threshold for the factor loading (default 0.4)"));
    System.out.print(String.format("%s\t%s\n", "-p <int>", "printing: short(Table 4 only) or long(Tables 1..4); default short"));
    System.out.print(String.format("%s\t%s\n", "-t <decimal fraction>", "threshold below which output is suppressed in output tables    (default 0.2)"));
    System.out.print(String.format("%s\t%s\n", "-v <decimal fraction>", "percent variance explained (default 90%) "));
    System.out.print("Toggle ON:");
    System.out.print(String.format("%s\t%s\n", "-g", "analyze by genotype (default haplotype)"));
    System.out.print(String.format("%s\t%s\n", "-h", "show this help and exit"));
    System.out.print(String.format("%s\t%s\n", "-s", "if subset analyses required (default ignores subgroup column)\n\tmultiple output files are created with extensions as denoted in column"));
    System.out.print(String.format("%s\t%s\n", "-u", "UI mode"));
    System.exit(0);
  }

  public PCATagView( boolean uiMode,
		     boolean genotype,
		     boolean subsetAnalysis,
		     boolean longPrintMode,
		     String inputFileName,
		     String outputFileName,
		     double thresholdFactor,
		     double outputSupressThreshold,
		     double eigenValueThershold,  
		     double varianceExplained)
  {
    if (uiMode)
      {
	m_frame = new ExitableApplicationFrame(UIConstants.s_PCA_TAG);
	createUI(genotype,
		 subsetAnalysis,
		 longPrintMode,
		 inputFileName,
		 outputFileName,
		 thresholdFactor,
		 outputSupressThreshold,
		 eigenValueThershold,
		 varianceExplained);
      }
    else
      {
	if (inputFileName != null && outputFileName != null)
	  {
	    PCATagRunner runner = new PCATagRunner(inputFileName,
						   outputFileName);
	    runner.start();
	  }
	else
	  {
	    showHelp();
	  }
      }
  }

  private void createUI(boolean genotype,
                        boolean subsetAnalysis,
                        boolean longPrintMode,
                        String inputFileName,
                        String outputFileName,
                        double thresholdFactor,
                        double outputSupressThreshold,
                        double eigenValueThershold,
                        double varianceExplained) {
    JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    {
      JPanel topComponent = new JPanel();
      {
        topComponent.setLayout(new VerticalLayout());
        m_ioPanel = new InputOutputFilesPanel(this,
					      inputFileName,
					      outputFileName,
					      longPrintMode);
        topComponent.add(m_ioPanel);
        m_optionsPanel = new OptionsPanel(genotype,
					  subsetAnalysis,
					  thresholdFactor,
					  outputSupressThreshold,
					  eigenValueThershold,
					  varianceExplained);
        topComponent.add(m_optionsPanel);
        splitPane.setTopComponent(new JScrollPane(topComponent));
      }

      JScrollPane pane = new JScrollPane();
      {
        area = new JTextArea();
        area.setEditable(false);
        PrintStream out = new PrintStream(new TextAreaOutputStream(area));

        // redirect standard output stream to the TextAreaOutputStream
        System.setOut(out);

        // redirect standard error stream to the TextAreaOutputStream
        System.setErr(out);
        pane.setViewportView(area);
        splitPane.setBottomComponent(pane);
      }
      splitPane.setOneTouchExpandable(true);
      splitPane.setDividerSize(15);
    }
    m_frame.getContentPane().add(splitPane);
    m_frame.pack();
    m_frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
  }

  private static void setNativeLookAndFeel() {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } 
    catch (Exception e) {
    }
  }

  public void clearOutput() {
    area.setText("");
  }

  public void runPCATag(String inputFileName, String outputFileName) {
    PCATagRunner runner = new PCATagRunner(inputFileName, outputFileName);
    runner.start();
  }

  public LinkedHashMap<String, Locus[]>[] validateInput() {
    readParametersFromUI();
    LinkedHashMap<String, Locus[]>[] data = new LinkedHashMap[3];
    if (! m_inputFileName.startsWith("/")) {
      m_inputFileName = m_workingDirectory + File.separator + m_inputFileName;
    }
    PCATagFileReader reader = new PCATagFileReader(m_inputFileName);
    data = reader.readTextFile(m_requiresSubsetAnalysis);

    return data;
  }

  public void readParametersFromUI() {
    if (m_uiMode) {
      m_inputFileName = m_ioPanel.getInputFileName();
      m_longPrintMode = m_ioPanel.isLongOutput();
      m_outputFileName = m_ioPanel.getOutputFileName();
      m_analizeGenotype = m_optionsPanel.isGenotype();
      m_eigenValueThershold = m_optionsPanel.getEigenValue();
      m_outputSupressThreshold = m_optionsPanel.getOutputSupressThreshold();
      m_requiresSubsetAnalysis = m_optionsPanel.isSubset();
      m_thresholdFactor = m_optionsPanel.getRetain();
      m_varianceExplained = m_optionsPanel.getVarianceExplained();
      clearOutput();
    }

  }

  public class PCATagRunner extends Thread {
    private String m_inputFileName = null;
    private String m_outputFileName = null;
    
    /**
     *
     */
    public PCATagRunner( String inputFileName, String outputFileName) {
      m_inputFileName = inputFileName;
      m_outputFileName = outputFileName;
    }

    @Override
    public void run() {
      try {
        if (m_uiMode) {
          m_frame.getComponent().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        }

        if (validateInput() != null) {
          processInput();
        }
      } catch (Exception e) {
        System.out.println("Warning: Execution completed with errors. ");
        e.printStackTrace();
      } finally {
        if (m_uiMode) {
          m_frame.getComponent().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
      }
    }

    public void processInput() {
      readParametersFromUI();

      if (m_outputFileName.equals(m_inputFileName)) {
	System.out.println("Input and output files cannot be the same.");
      }

      LinkedHashMap<String, Locus[]>[] data = validateInput();

      if (!(data == null)) {
        DataAnalyzer analyzer = new DataAnalyzer(data, m_requiresSubsetAnalysis);

        if (!analyzer.isValid()) {
          analyzer.printError();
          return;
        }
        analyzer.loadSilos();
        createFreqFile(analyzer);

        System.out.println("File validation is complete.");

        if (m_analizeGenotype) {
          int runs = 0;

	  if (m_requiresSubsetAnalysis) {
	    runs = 3;
	  }
	  else {
	    runs = 1;
	  }

          for (int i = 0; i < runs; i++) {
            System.out.println("Generating genotype frequency file...");
            ArrayList<String> returnValues = analyzer.createGenotypeFreqFile(data[i]);
            String freqFileName = returnValues.get(0);

            System.out.println("Frequency file sucessfully written to: " + m_workingDirectory + File.separator + "freq.txt");
            System.out.println("Executing R...");

            int numMarkers = Integer.parseInt(returnValues.get(1));
            RRunner rRun = new RRunner(new File(freqFileName),
				       numMarkers,
				       m_eigenValueThershold,
				       m_thresholdFactor,
				       m_outputSupressThreshold,
				       m_outputFileName,
				       !m_longPrintMode,
				       m_varianceExplained,
				       i);

            boolean success = rRun.run(analyzer);
            if (success) {
              System.out.println("R execution complete. Output file has been successfully written to: " + m_outputFileName);
            }
            else {
              System.out.println("Warning: R execution completed with errors. ");
            }
          }
        }
        else {
          // phase then R
          ArrayList<String> createdFiles = analyzer.generatePhaseFile();
          for (int i = 0; i < createdFiles.size(); i++) {
            System.out.println("Executing PHASE. Please be patient as this may take a while...");

            // Run PHASE
            PhaseRunner phaseRunnable = new PhaseRunner(createdFiles.get(i));
            String phaseOutPrefix = phaseRunnable.startPhase();
            System.out.println("PHASE execution complete.");

            // Transform phase to R format
            PhaseToRTransformation p2R = new PhaseToRTransformation(m_outputFileName, analyzer.getMakerNamesForIndex(i));
            System.out.println("Executing R...");
            File orginalOutput = new File(m_outputFileName);
            ArrayList<String> returnValues = p2R.runTransformation();
            String transformedFreqFileName = returnValues.get(0);
            int numMarkers = Integer.parseInt(returnValues.get(1));

            // Run R
            RRunner rRun = new RRunner(new File(transformedFreqFileName),
				       numMarkers,
				       m_eigenValueThershold,
				       m_thresholdFactor,
				       m_outputSupressThreshold,
				       orginalOutput.getAbsolutePath(),
				       !m_longPrintMode,
				       m_varianceExplained,
				       i);
            boolean success = rRun.run(analyzer);

	    if (success) {
	      System.out.println("R execution complete. Output file has been successfully written to: "
				 + orginalOutput.getAbsolutePath());
	    }
	    else {
	      System.out.println("Warning: R execution completed with errors. ");
	    }
          }
        }
      }
    }

    public void createFreqFile( DataAnalyzer analyzer) {


      if (analyzer.getNumOfMarkers() == 0 ) {
	throw new RuntimeException("No markers in analyser");
      }
      String result = null;
      String subset = UIConstants.pca_OVERALL;
      PrintWriter outputStream = null;
      try {
        File file = new File(m_outputFileName);
	File outFile;
	if (file.isDirectory()) {
	  outFile = new File(m_outputFileName,"out.freq");
          m_outputFileName = outFile.getName();
	}
	else {
	  outFile = new File(m_outputFileName);
	}
	FileWriter outputFileWriter = new FileWriter(outFile);
        outputStream = new PrintWriter(outputFileWriter);

        System.out.println("Creating Frequency Output File...\n");
        System.out.println("Marker" + "\t" + "Subset" + "\t" + "Valid"
			   + "\t" + "Missing" + "\t" + "Major" + "\t" + "Freq" + "\t"
			   + "Minor" + "\t" + "freq");

        outputStream.println("Marker" + "\t" + "Subset" + "\t"
			     + "Valid" + "\t" + "Missing" + "\t" + "Major" + "\t"
			     + "Freq" + "\t" + "Minor" + "\t" + "freq");

        for (int j = 0; j < analyzer.getNumOfMarkers(); j++) {
          for (int i = 0; i < 3; i++) {
            if (analyzer.getvalidMarkerArr()[i] == null) {
              continue;
            }

            switch (i) {
	    case 0:
	      subset = UIConstants.pca_OVERALL;
	      break;
	    case 1:
	      subset = UIConstants.pca_CONTROL;
	      break;
	    case 2:
	      subset = UIConstants.pca_CASE;
	      break;
	    default:
	      subset = "unsure";
            }

            result = analyzer.getMarkerNames()[j] + "\t" 
	      + subset + "\t" 
	      + analyzer.getvalidMarkerArr()[i][j] + "\t"
              + analyzer.getmissingMarkerArr()[i][j] + "\t"
              + analyzer.getmajorAlleleArr()[i][j] + "\t"
              + analyzer.getmajorFreqArr()[i][j] + "\t"
              + analyzer.getminorAlleleArr()[i][j] + "\t"
              + analyzer.getminorFreqArr()[i][j] + "\t";

            outputStream.println(result);
            System.out.println(result);

          }

          if (!subset.equalsIgnoreCase(UIConstants.pca_OVERALL)) {
            outputStream.println();
            System.out.println();
          }
        }
        outputStream.close();
        System.out.println("\nFrequency file has been successfully written to: " + outFile.getName());
      }
      // Catches any error conditions
      catch (IOException e) {
        System.err.println("Unable to write frequency file.");
        System.exit(-1);
      }
    }
  }

  public JFrame getFrame() {
    return m_frame;
  }
}
