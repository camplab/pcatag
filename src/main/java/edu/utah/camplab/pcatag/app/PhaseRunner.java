package edu.utah.camplab.pcatag.app;

import java.io.IOException;

import edu.utah.camplab.pcatag.utils.StreamReaderThread;


public class PhaseRunner {
  String m_inputFile = null;
  String m_outputFilePrefix = "fp123";

  @SuppressWarnings("unused")
  private PhaseRunner() {

  }

  public PhaseRunner(String inputFile) {
    m_inputFile = inputFile;
    //m_outputFilePrefix = inputFile.replace("_in", "_out_freqs");
  }

  @SuppressWarnings({"null", "null"})
  public String startPhase() {
    try {
      Runtime runtime = Runtime.getRuntime();
      String command = PCATagView.s_PHASE_EXECUTABLE_FILE_NAME + " -o" + m_outputFilePrefix + " " + m_inputFile ;
      System.out.println(command);
      Process proc = runtime.exec(command);
      StreamReaderThread outThread = new StreamReaderThread(proc.getInputStream(), new StringBuffer());
      StreamReaderThread errThread = new StreamReaderThread(proc.getErrorStream(), new StringBuffer());

      // start both threads
      outThread.start();
      errThread.start();

      // wait for process to end
      proc.waitFor();

      // finish reading whatever's left in the buffers
      outThread.join();
      errThread.join();
	/*
	  String errorString = errThread.m_outBuffer.toString();
	  if (errorString.length() > 0)
	  {
	  System.out.println(StringUtils.LINE_SEPARATOR + errorString);
	  throw new RuntimeException();
	  }
	*/
      // File fileToDelete = new File(m_outputFile);
      // fileToDelete.delete();
      // fileToDelete = new File(m_outputFile + "_hbg");
      // fileToDelete.delete();
      // fileToDelete = new File(m_outputFile + "_monitor");
      // fileToDelete.delete();
      // fileToDelete = new File(m_outputFile + "_probs");
      // fileToDelete.delete();
      // fileToDelete = new File(m_outputFile + "_pairs");
      // fileToDelete.delete();
      // fileToDelete = new File(m_outputFile + "_recom");
      // fileToDelete.delete();
      // fileToDelete = new File(m_inputFile);
      // fileToDelete.delete();

      return m_outputFilePrefix;
    } catch (InterruptedException ie) {
      throw new RuntimeException(ie);
    } catch (IOException ioe) {
      throw new RuntimeException(ioe);
    }
  }
}
