package edu.utah.camplab.pcatag.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import edu.utah.camplab.pcatag.utils.StringUtils;


public class PhaseToRTransformation {
  private String m_phaseFreqFile = null;
  private ArrayList<String> m_markerNames = null;

  /**
   * @param phaseFreqFile
   */
  public PhaseToRTransformation(String phaseFreqFile, ArrayList<String> markerNames) {
    m_phaseFreqFile = phaseFreqFile;
    m_markerNames = markerNames;
  }

  @SuppressWarnings("null")
  public ArrayList<String> runTransformation() {
    ArrayList<String> returnList = new ArrayList<String>();

    String transformedFileName = null;
    String numberOfMarkers = "0";

    FileWriter fw = null;
    ArrayList<ArrayList<String>> freqFileRows = new ArrayList<ArrayList<String>>();
    try {
      BufferedReader br = new BufferedReader(new FileReader(m_phaseFreqFile));
      String line = null;
      boolean firstLine = true;

      while ((line = br.readLine()) != null) {
        if (firstLine) {
          firstLine = false;
        }
        else {
          line = line.trim();
          String parsedLine[] = line.split("\\s");
          ArrayList<String> nonEmptyValues = new ArrayList<String>();
          for (int i = 0; i < parsedLine.length; i++) {
            if (parsedLine[i].length() > 0) {
              nonEmptyValues.add(parsedLine[i]);
            }
          }
          freqFileRows.add(nonEmptyValues);
        }
      }

      transformedFileName = m_phaseFreqFile + "transformed";
      fw = new FileWriter(new File(transformedFileName));

      // first column
      String columnNames = "";
      for (int i = 0; i < m_markerNames.size(); i++) {
          columnNames += (m_markerNames.get(i) + " ");
      }
      columnNames += "Efreq SE";
      fw.write(columnNames + StringUtils.LINE_SEPARATOR);
      int columns = freqFileRows.get(1).size();
      for (int c = 1; c < columns; c++) { // Start at one, we don't need the marker name again
          for (int i = 0; i < freqFileRows.size(); i++) {
              fw.append(freqFileRows.get(i).get(c) + " ");
          }
        fw.append("0 0\n");
      }
    }
    catch (IOException ioe) {
      ioe.printStackTrace();
    }
    finally {
      try {
        fw.close();
      } catch (IOException ioe1) {

      }
    }
    returnList.add(transformedFileName);
    returnList.add(numberOfMarkers);
    return returnList;
  }
}
