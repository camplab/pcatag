package edu.utah.camplab.pcatag.app;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;


public class PCATagFileReader {
  private String m_inputFileName = null;
  private static Pattern splitter = Pattern.compile("[0-9]");

  /**
   * @param inputFileName
   */
  public PCATagFileReader(String inputFileName) {
    m_inputFileName = inputFileName;
  }

  public LinkedHashMap<String, Locus[]>[] readTextFile(boolean subSetAnalysis) {
    int lineNum = 0;

    try {
      // input/output file names
      String inputFileName = m_inputFileName;

      // Create FileReader Object
      FileReader inputFileReader = new FileReader(inputFileName);

      // Create Buffered/PrintWriter Objects
      BufferedReader inputStream = new BufferedReader(inputFileReader);

      // Keep in mind that all of the above statements can be combined
      // into the following:
      // BufferedReader inputStream = new BufferedReader(new
      // FileReader("README_InputFile.txt"));
      // PrintWriter outputStream = new PrintWriter(new
      // FileWriter("ReadWriteTextFile.out"));

      String inLine = null;
      String ID = null;
      String temp = null;

      boolean subsetExist = false;
      int extraSpace = 0;
      Integer fileNum = 0;
      String[] result = null;
      String[] header = null;
      String[] tempArr = null;

      LinkedHashMap<String, Locus[]> list = new LinkedHashMap<String, Locus[]>();
      LinkedHashMap<String, Locus[]> list2 = new LinkedHashMap<String, Locus[]>();
      LinkedHashMap<String, Locus[]> list3 = new LinkedHashMap<String, Locus[]>();

      Locus[] markerArr = null;

      while ((inLine = inputStream.readLine()) != null) {
        int i = 0;

        // store header info
        if (lineNum == 0) {
          header = inLine.split("\\s+");
          if (header[header.length - 1].equals("Subset")) {
            subsetExist = true;
          }
        }

        markerArr = new Locus[header.length - 1];
        if (lineNum > 0) {

          if (subsetExist == true) {
            inLine = inLine.trim();
          }

          // result=splitter.split(inLine);

          tempArr = inLine.split("\\s+", 2);
          temp = new String(tempArr[0] + " " + tempArr[1]);
          temp = replaceSpace(temp, subsetExist, lineNum);
          if (temp.contains("\t")) {
            temp = temp.replace("\t0", "\t0\t");
          }
          result = temp.split("\\s");

          ID = result[0];

          if (subSetAnalysis == true && subsetExist == true) {
            if (result[result.length - 1] == "") {
              System.out.println("Subset column was detected, but no value exists. Error encountered on line: "
                + lineNum + ".");
              return null;
            }

            fileNum = Integer.valueOf(result[result.length - 1]);
            result[result.length - 1] = null;

          }
          else {
            if (subSetAnalysis == true && subsetExist == false) {
              System.out
                .println("File does not have subset analysis column.");
              return null;
            }

          }

          // delete extra white space
          if (subsetExist == false) {
            extraSpace = (result.length - 1) - (((header.length - 1) * 2) + 1);
          }

          // Check for subordinats
          // if false put null

          // check if number of columns/Markers is correct

          // end

          // Add result to the double array of markers
          for (int x = 1; x < result.length - 1 - extraSpace; x = x + 2) {
            // check if Marker has one value is empty
            // exit if yes

            // end

            if (result[x].equals("")) {
              x++;
            }

            Locus m = new Locus(header[i + 1],
              result[x],
              result[x + 1]);

            // if missing alleles then put 0s

            HandleMissingAlleles(m);
            // end

            markerArr[i] = m;
            i = i + 1;
          }

          // if subordinats is true create multiple linked Hash
          // Map
          if (subSetAnalysis == true && subsetExist == true) {
            switch (fileNum.intValue()) {
              case 0:
                list.put(ID,
                  markerArr);
                break;
              case 1:
                list2.put(ID,
                  markerArr);
                break;
              case 2:
                list3.put(ID,
                  markerArr);
                break;
            }
          }
          else {
            list.put(ID,
              markerArr);
          }

        }
        lineNum = lineNum + 1;
      }

      if (!subSetAnalysis) {
        System.out.println("Reading input file...");
        // DataAnalyzer.printHashedMap(list);
      }
      else {
        System.out.println("Reading input file using subset analysis.");
        System.out.println("Overall: ");
        //DataAnalyzer.printHashedMap(list);
        System.out.println("Control: ");
        //DataAnalyzer.printHashedMap(list2);
        System.out.println("Case: ");
        //DataAnalyzer.printHashedMap(list3);
      }

      /*
       * outputStream.println(); outputStream.println("+---------- Testing
       * output to a file ----------+");
       */

      inputStream.close();

      // System.out.println(InputValidation.Validate(list, null,
      // null));

      LinkedHashMap<String, Locus[]>[] lists;
      lists = new LinkedHashMap[3];
      lists[0] = list;
      lists[1] = list2;
      lists[2] = list3;

      return lists;

    } catch (Exception e) {
      System.out.println("File is not valid. Error encountered on line: "
        + lineNum + ".");
      // System.out.println("IOException:");
      e.printStackTrace();
      return null;
    }

  }

  private void HandleMissingAlleles(
    Locus m) {

    // If both alleles are missing replace values with 0

    if ((m.GetAllele1().equals("") == true && m.GetAllele2().equals("") == true)) {
      m.SetAllele1("0");
      m.SetAllele2("0");
    }

    // If both alleles are missing replace values with 0
    if ((m.GetAllele1().equals("0") == true || m.GetAllele2().equals("0") == true)) {
      m.SetAllele1("0");
      m.SetAllele2("0");
    }
  }

  private String replaceSpace(
    String str,
    boolean subsetExist,
    int lineNum) {

    char[] charArray = str.toCharArray();
    String result = null;

    for (int i = 0; i < charArray.length - 1; i++) {
      if (charArray[i] == charArray[i + 1]
        && (charArray[i] == ' ' || charArray[i] == '\t')) {
        charArray[i + 1] = '0';
        i = i + 1;
      }
    }
    result = new String(charArray);
    return result;
  }

}
