# Installation 


**Download and Install:**

        Step1: Click on Get PCAtag  on the left pane of the home page to download PCAtag.zip  
        Step2: unzip PCAtag.zip to your local directory   
        Step3: [Click here](Files.zip) to download sample files 

  
**Running PCAtag:**

       **Start PCAtag in GUI mode:**

*   Double click on the jar file (PCAtag.jar)

        **Start PCAtag from the command prompt:**      

       Step1: From the command prompt go to the directory where you extracted PCAtag.zip  
       Step2: type "java -jar PCAtag.jar"   

 From the Command Line, the following command line parameters are supported:

*   ��i� <input file name>
*   ��o� <output file name>
*   ��g� for genotype, default is �haplotype�
*   �-s� for subset analysis (separate analyses for each subset), default is �no subset analysis� (one analysis that considers all the data)
*   �-e� <Eigenvalue threshold \[0.0..1.0\]>, default is � 0.7�
*   �-v� <variance explained \[0..100%\]>, default value is �90%�
*   ��f � <factor loading threshold for group membership \[0.0-1.0\]>� , default value is � 0.4�
*   �-t� < suppression for printing \[0.0-1.0\]> threshold below which the factor loading is not printed in the output, default value is � 0.2�
*   �-p� <short | long> short or long output print mode, default is �short�  
    
     short = Table 4 only
    
     long = Tables 1, 2 , 3 and 4 
    
*   � \-2� for two step PCA analysis option, default multi-step

      Example of appropriate syntax:

      _java -jar PCAtag.jar -i "C:\\PCAtag\\Input.txt" -o "C:\\PCAtag\\Output.txt"_

**System Requirements:**

*     1 Gb free disk space
    
*     Java 1.8 or newer
